package com.example.demo.api;

import java.util.Arrays;
import java.util.stream.IntStream;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class Ejercicios {
 
	@PostMapping(value="/api/ejercicio1",consumes = "application/json", produces = "application/json")
	public int[] primerEjercicio(@RequestBody int[] array) {
		
//		Crear una función que reciba como parámetro un arreglo de números enteros positivos en cualquier
//		orden, el algoritmo debe completar si faltan números en el arreglo en el rango dado. Finalmente
//		devolver el arreglo completo y ordenado de manera ascendente.
		
		Arrays.sort(array);
		int max = array[array.length - 1];//Obtain the maximum number of the array
		int [] result = IntStream.range(1, max+1).toArray(); //Fulfill an array in the range of 1 to max number.
		return result;
	}
	@PostMapping(value="/api/ejercicio2",consumes = "application/json", produces = "application/json")
	public String segundoEjercicio(@RequestBody String cadena) {
		
//		Crear una función que reciba un parámetro string que reemplace cada letra de la cadena con la letra
//		siguiente en el alfabeto. Por ejemplo, reemplazar a por b ó c por d. Finalmente devolver la cadena.
		String foo = cadena;
		String result = "";

		for (char c : foo.toCharArray()) {
			if (Character.isLetter(c)) {//Check if the character is a Letter
				if(Character.isUpperCase(c)) {//Check if the character is upperCase
					result += Character.toString((char) (((c - 'A' + 1) % 26) + 'A')); //Adds the next letter
				}else {
					result += Character.toString((char) (((c - 'a' + 1) % 26) + 'a')); //Adds the next letter
				}
			}else {
				result +=c;
			}
		}
		return result;
		
	}
}
