# Demo API Springboot 4 


cuenta con dos ejercicios para probar el despliegue y ejecución de dos funciones que fueron solicitadas.

## Caracteristicas

La API demo fue montado sobre framework de springboot STS 4 + Java 1.8. 

## Motivación
Demostrar el conocimiento técnico requerido para montar una API sencilla que permita devolver los valores que fueron solicitados en los ejercicios

## Requerimientos
Para lanzar el proyecto se necesita:

+ [Springboot STS 4](https://spring.io/tools)
+ [Java 1.8](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)
+ [Postman](https://www.postman.com)


## Getting Started

	git clone https://gitlab.com/pabloarriazamillar/demo-ejercicios.git
    Importar el proyecto a la intancia de Spring Tool Suite 4
    Run As -> Spring Boot App

## Modo de uso

Con el programa ya corriendo, debemos abrir Postman e importar el archivo "test.postman_collection_ejercicios" 
que se encuentra en la carpeta: demo/resources/postman - collection.

En esta colección se podrán encontrar 2 request tipo POST:

1.- **Ejercicio1:** Envia un Array de números positivos al azar y la función devuelve un nuevo Array ordenado de forma ascendente con los números del Array de entrada junto a los números faltantes entre estos.

- Array de ejemplo enviado: [10,20].
- Array devuelto: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20].


2.- **Ejercicio2:** Envia un String con letras y otros caracteres. La función devuelve un String con los caracteres en el mismo orden pero cada letra corresponde a la siguiente en el abecedario.

- String de ejemplo enviado: **Casa 52.
- String recibido: **Dbtb 52.


## Licencia
license ([MIT](http://opensource.org/licenses/mit-license.php))
